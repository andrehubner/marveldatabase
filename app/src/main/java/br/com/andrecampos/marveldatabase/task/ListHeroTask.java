package br.com.andrecampos.marveldatabase.task;

import android.content.Context;
import android.text.TextUtils;

import br.com.andrecampos.marveldatabase.webservice.WebServiceFactory;
import br.com.andrecampos.marveldatabase.webservice.interfaces.HeroWebService;
import br.com.andrecampos.marveldatabase.webservice.response.HeroResponse;
import retrofit2.Response;

/**
 * -- MarvelDatabase --
 * Autor: André Campos
 * E-mail: andrecampos.ds@gmail.com
 * Data: 29/09/16 - 09:07
 */

public class ListHeroTask extends BaseTask<HeroResponse> {

    private int offset;
    private String text;

    public ListHeroTask(Context context, String text, int offset) {
        // Executa o método construtor da classe ancestral
        super(context);
        // Define o valor dos atributos do objeto
        this.text = text;
        this.offset = offset;
    }

    @Override
    protected HeroResponse doInBackground(Void... voids) {
        // Instancia o objeto responsável por realizar a comunicação com o webservice
        HeroWebService webService = WebServiceFactory.build(HeroWebService.class);
        // Define a variável que irá receber o retorno do webservice
        Response<HeroResponse> response;
        // Inicia um bloco seguro para realizar a chamada
        try {
            // Verifica se o texto foi informado
            if (!TextUtils.isEmpty(this.text)) {
                // Executa a chamada do webservice
                response = webService.findByName(this.text,
                        WebServiceFactory.createWebServiceParameters(this.getContext(), this.offset))
                        .execute();
            }
            else {
                // Executa a chamada do webservice
                response = webService.list(WebServiceFactory.createWebServiceParameters(this.getContext(),
                        this.offset))
                        .execute();
            }
        } catch (Exception e) {
            // Imprime a pilha de exceções
            e.printStackTrace();
            // Retorna nulo, indicando que a função não foi realizada com sucesso
            return null;
        }
        // Verifica se ocorreu algum erro durante a execução do método
        if (!response.isSuccessful()) {
            // Retorna nulo, indicando que a função não foi realizada com sucesso
            return null;
        }
        // Retorna o objeto de response que possui os dados dos heróis
        return response.body();
    }
}
