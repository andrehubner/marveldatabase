package br.com.andrecampos.marveldatabase.util;

import java.lang.reflect.Array;
import java.util.Date;
import java.util.List;

/**
 * -- MarvelDatabase --
 * Autor: André Campos
 * E-mail: andrecampos.ds@gmail.com
 * Data: 29/09/16 - 15:54
 */

public class ParcelUtils {

    private ParcelUtils() {
        // Construtor privado para impedir que a classe seja instanciada
    }

    public static Boolean readBoolean(int value) {
        // Retorna o valor da função
        return value == 1;
    }

    public static Date readDate(long value) {
        // Cria a variável para receber o valor numérico
        Long longData = readLong(value);
        // Cria a variável que será retornada pela função
        Date retorno = null;
        // Verifica se o valor foi recuperado com sucesso
        if (longData != null) {
            // Instancia um novo objeto de data
            retorno = new Date(longData);
        }
        // Retorna o valor da função
        return retorno;
    }

    public static Float readFloat(float value) {
        // Cria a variável que será retornada pela função
        Float retorno = null;
        // Verifica se o valor informado é diferente de zero
        if (value != 0) {
            // Atribui o valor à variável de retorno
            retorno = value;
        }
        // Retorna o valor da função
        return retorno;
    }

    public static Integer readInt(int value) {
        // Cria a variável que será retornada pela função
        Integer retorno = null;
        // Verifica se o valor informado é diferente de zero
        if (value != 0) {
            // Atribui o valor à variável de retorno
            retorno = value;
        }
        // Retorna o valor da função
        return retorno;
    }

    public static Long readLong(long value) {
        // Cria a variável que será retornada pela função
        Long retorno = null;
        // Verifica se o valor informado é diferente de zero
        if (value != 0L) {
            // Atribui o valor à variável de retorno
            retorno = value;
        }
        // Retorna o valor da função
        return retorno;
    }

    public static int writeBoolean(Boolean value) {
        // Retorna o valor da função
        return ((value != null) && (value)) ? 1 : 0;
    }

    public static long writeDate(Date data) {
        // Cria a variável que será retornada pela função
        long retorno = 0L;
        // Verifica se a data foi informada
        if (data != null) {
            // Atribui o valor numérico da data
            retorno = data.getTime();
        }
        // Retorna o valor da função
        return retorno;
    }

    public static float writeFloat(Float value) {
        // Cria a variável que será retornada pela função
        float retorno = 0;
        // Verifica se o valor informado não é nulo
        if (value != null) {
            // Atribui o valor à variável de retorno
            retorno = value;
        }
        // Retorna o valor da função
        return retorno;
    }

    public static int writeInt(Integer value) {
        // Cria a variável que será retornada pela função
        int retorno = 0;
        // Verifica se o valor informado não é nulo
        if (value != null) {
            // Atribui o valor à variável de retorno
            retorno = value;
        }
        // Retorna o valor da função
        return retorno;
    }

    public static long writeLong(Long value) {
        // Cria a variável que será retornada pela função
        long retorno = 0L;
        // Verifica se o valor informado não é nulo
        if (value != null) {
            // Atribui o valor à variável de retorno
            retorno = value;
        }
        // Retorna o valor da função
        return retorno;
    }

    @SuppressWarnings("unchecked")
    public static <T> T[] writeTypedArray(List<T> lista, Class<T> classe) {
        // Verifica se a classe foi informada
        if (classe != null) {
            // Recupera o tamanho que o array deve possuir
            final int tamanho = lista != null ? lista.size() : 0;
            // Cria uma nova instância do array
            T[] retorno = (T[]) Array.newInstance(classe, tamanho);
            // Verifica se a lista possui registros
            if (tamanho > 0) {
                // Insere os registros no array
                lista.toArray(retorno);
            }
            // Retorna o array criado
            return retorno;
        }
        // Retorna nulo, indicando que não foi possível realizar criar o array
        return null;
    }
}
