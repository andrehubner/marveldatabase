package br.com.andrecampos.marveldatabase.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

/**
 * -- MarvelDatabase --
 * Autor: André Campos
 * E-mail: andrecampos.ds@gmail.com
 * Data: 29/09/16 - 00:30
 */

public abstract class BaseFragment extends Fragment {

    public BaseFragment() {
        // Método construtor vazio conforme documentação da Google.
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        // Executa o método da classe ancestral
        super.onCreate(savedInstanceState);
        // Configura o fragmento para reter a instância ao destruir a activity durante
        // a alteração de configurações
        this.setRetainInstance(true);
    }
}
