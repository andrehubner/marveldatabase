package br.com.andrecampos.marveldatabase.model;

import android.os.Parcel;

import com.google.gson.annotations.Expose;
import com.orm.dsl.Table;

import br.com.andrecampos.marveldatabase.util.ParcelUtils;

/**
 * -- MarvelDatabase --
 * Autor: André Campos
 * E-mail: andrecampos.ds@gmail.com
 * Data: 28/09/16 - 23:49
 */

@Table
public class Thumbnail extends BaseModel {

    private Long id;
    @Expose
    private String path;
    @Expose
    private String extension;

    public Thumbnail() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getUrl() {
        return this.getPath() + "/standard_large." + this.getExtension();
    }

    protected Thumbnail(Parcel source) {
        // Executa o método construtor da classe ancestral
        super(source);
        // Define o valor dos atributos a partir do objeto
        this.id = ParcelUtils.readLong(source.readLong());
        this.path = source.readString();
        this.extension = source.readString();
    }

    public static final Creator<Thumbnail> CREATOR = new Creator<Thumbnail>() {

        @Override
        public Thumbnail createFromParcel(Parcel parcel) {
            // Retorna uma nova instância de thumbnail
            return new Thumbnail(parcel);
        }

        @Override
        public Thumbnail[] newArray(int size) {
            // Retorna um array de heróis do tamanho solicitado
            return new Thumbnail[size];
        }
    };

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        // Executa o método da classe ancestral
        super.writeToParcel(parcel, flags);
        // Adiciona os atributos na parcela
        parcel.writeLong(ParcelUtils.writeLong(this.id));
        parcel.writeString(this.path);
        parcel.writeString(this.extension);
    }

    @Override
    public int describeContents() {
        // Retorna um valor únicio para a classe
        return 200;
    }
}
