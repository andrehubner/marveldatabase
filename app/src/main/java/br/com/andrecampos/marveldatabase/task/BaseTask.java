package br.com.andrecampos.marveldatabase.task;

import android.content.Context;
import android.os.AsyncTask;

/**
 * -- MarvelDatabase --
 * Autor: André Campos
 * E-mail: andrecampos.ds@gmail.com
 * Data: 29/09/16 - 08:57
 */

public abstract class BaseTask<T> extends AsyncTask<Void, Void, T> {

    private Context context;

    private OnPreExecuteListener onPreExecuteListener;
    private OnPostExecuteListener<T> onPostExecuteListener;
    private OnFinishedListener onFinishedListener;

    public BaseTask(Context context) {
        // Executa o método construtor da classe ancestral
        super();
        // Define o valor dos atributos do objeto
        this.context = context;
    }

    public Context getContext() {
        // Retorna o contexto informado
        return this.context;
    }

    public void setOnPreExecuteListener(OnPreExecuteListener onPreExecuteListener) {
        // Atribui o listener informado
        this.onPreExecuteListener = onPreExecuteListener;
    }

    public void setOnPostExecuteListener(OnPostExecuteListener<T> onPostExecuteListener) {
        // Atribui o listener informado
        this.onPostExecuteListener = onPostExecuteListener;
    }

    public void setOnFinishedListener(OnFinishedListener onFinishedListener) {
        // Atribui o listener informado
        this.onFinishedListener = onFinishedListener;
    }

    @Override
    protected void onPreExecute() {
        // Executa o método da classe ancestral
        super.onPreExecute();
        // Verifica se o listener foi instanciado
        if (this.onPreExecuteListener != null) {
            // Executa o método "OnPreExecute" do listener
            this.onPreExecuteListener.onPreExecute();
        }
    }

    @Override
    protected void onPostExecute(T result) {
        // Executa o método da classe ancestral
        super.onPostExecute(result);
        // Verifica se o listener foi instanciado
        if (this.onPostExecuteListener != null) {
            // Executa o método "onPostExecute" do listener
            this.onPostExecuteListener.onPostExecute(result);
        }
        // Verifica se o listener foi instanciado
        if (this.onFinishedListener != null) {
            // Executa o método "onFinished" do listener
            this.onFinishedListener.onFinished();
        }
    }

    @Override
    protected void onCancelled() {
        // Executa o método da classe ancestral
        super.onCancelled();
        // Verifica se o listener foi instanciado
        if (this.onFinishedListener != null) {
            // Executa o método "onFinished" do listener
            this.onFinishedListener.onFinished();
        }
    }

    public interface OnPreExecuteListener {
        void onPreExecute();
    }

    public interface OnPostExecuteListener<T> {
        void onPostExecute(T result);
    }

    public interface OnFinishedListener {
        void onFinished();
    }
}
