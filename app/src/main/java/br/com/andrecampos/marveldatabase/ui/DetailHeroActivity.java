package br.com.andrecampos.marveldatabase.ui;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.graphics.Palette;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.BitmapImageViewTarget;

import br.com.andrecampos.marveldatabase.R;
import br.com.andrecampos.marveldatabase.model.Hero;
import br.com.andrecampos.marveldatabase.ui.fragment.GalleryEventFragment;

public class DetailHeroActivity extends BaseActivity {

    public static final String TAG = DetailHeroActivity.class.getName();
    public static final String EXTRA_HERO = TAG + ".EXTRA_HERO";

    private CollapsingToolbarLayout collapsingToolbarLayout;
    private ImageView imgToolbar;
    private FloatingActionButton btnSave;
    private TextView txtDescription;
    private TextView txtModified;

    private Hero hero;
    private GalleryEventFragment galleryEventFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Executa o método da classe ancestral
        super.onCreate(savedInstanceState);
        // Renderiza o layout da activity
        setContentView(R.layout.activity_detail_hero);
        // Realiza a inicialização das views
        this.initViews();
        // Realiza as configurações do botão de salvar
        this.setupBtnSave();
        // Verifica se existe instância salva
        if (savedInstanceState != null) {
            // Tenta recuperar o herói salvo
            this.hero = savedInstanceState.getParcelable(EXTRA_HERO);
        }
        // Verifica se a intent foi informada e possui parâmetros
        if ((this.getIntent() != null) && (this.getIntent().getExtras() != null)) {
            // Verifica se o herói não foi recuperado
            if (this.hero == null) {
                // Recupera o herói da intent
                this.hero = this.getIntent().getParcelableExtra(EXTRA_HERO);
            }
        }
        // Realiza as configurações pertinentes ao herói
        this.setupHero();
        // Tenta recuperar o fragmento pelo ID
        this.galleryEventFragment = (GalleryEventFragment) this.getSupportFragmentManager()
                .findFragmentById(R.id.frgGalleryEvent);
        // Verifica se o fragmento foi instanciado
        if (this.galleryEventFragment != null) {
            // Atribui o ID do herói
            this.galleryEventFragment.setHeroId(this.hero.getId());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Renderiza o menu da tela
        this.getMenuInflater().inflate(R.menu.menu_detail_hero, menu);
        // Retorna o valor da função
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Recupera o ID do item selecionado
        int id = item.getItemId();
        // Verifica o ID do item selecionado
        switch (id) {
            case R.id.action_refresh:
                // Retorna o valor da função
                return true;
        }
        // Retorna o valor retornado pela classe ancestral
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        // Executa o método da classe ancestral
        super.onSaveInstanceState(outState);
        // Salva o herói da activity
        outState.putParcelable(EXTRA_HERO, this.hero);
    }

    /**
     * Inicializa as variáveis correspondentes às {@link android.view.View views} do layout.
     */
    private void initViews() {
        // Instancia a variável com os componentes definidos no layout da activity
        this.imgToolbar = (ImageView) this.findViewById(R.id.imgToolbar);
        this.collapsingToolbarLayout = (CollapsingToolbarLayout) this.findViewById(R.id.toolbar_layout);
        this.btnSave = (FloatingActionButton) this.findViewById(R.id.btnSave);
        this.txtDescription = (TextView) this.findViewById(R.id.txtDescription);
        this.txtModified = (TextView) this.findViewById(R.id.txtModified);
    }

    @Override
    protected void setupToolbar() {
        // Executa o método da classe ancestral
        super.setupToolbar();
        // Verifica se a toolbar foi instanciada
        if (this.getSupportActionBar() != null) {
            // Ativa o botão de voltar da ActionBar
            this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    private void setupBtnSave() {
        // Verifica se o botão foi instanciado
        if (this.btnSave != null) {
            // Atribui o evento de listener do botão
            this.btnSave.setOnClickListener(this.createSaveOnClickListener());
        }
    }

    private void refreshBtnSave() {
        // Verifica se o botão e o herói foram informados
        if ((this.btnSave != null) && (this.hero != null)) {
            // Define o ícone do botão
            btnSave.setImageResource(hero.isSaved()
                    ? R.drawable.ic_favorite_black_24dp
                    : R.drawable.ic_favorite_border_black_24dp);
        }
    }

    private View.OnClickListener createSaveOnClickListener() {
        // Retorna uma nova instância do listener de click
        return new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                // Verifica se o hero foi informado
                if (hero != null) {
                    // Inverte o valor da variável
                    hero.setSaved(!hero.isSaved());
                    // Atualiza o ícone do botão
                    refreshBtnSave();
                }
            }
        };
    }

    private void setupHero() {
        // Verifica se o herói foi instanciado
        if (this.hero != null) {
            // Verifica se é possível recuperar a ActionBar
            if (this.getSupportActionBar() != null) {
                // Atribui o nome do herói como título da página
                this.getSupportActionBar().setTitle(this.hero.getName());
            }
            // Atualiza o ícone do botão
            refreshBtnSave();
            // Atualiza a descrição do herói
            this.txtDescription.setText(!TextUtils.isEmpty(this.hero.getDescription())
                    ? this.hero.getDescription()
                    : this.getString(R.string.description_null));
            // Define a data de modificaçao
            this.txtModified.setText(this.hero.getModified() != null
                    ? android.text.format.DateUtils.formatDateTime(this,
                            this.hero.getModified().getTime(),
                            android.text.format.DateUtils.FORMAT_SHOW_DATE
                            | android.text.format.DateUtils.FORMAT_SHOW_TIME
                            | android.text.format.DateUtils.FORMAT_SHOW_YEAR)
                    : this.getString(R.string.modified_null));
            // Muda a imagem do botão de acordo com o parâmetro do herói
            this.btnSave.setImageResource(this.hero.isSaved()
                    ? R.drawable.ic_favorite_black_24dp
                    : R.drawable.ic_favorite_border_black_24dp);
            // Verifica se a ImageView da toolbar foi instanciada e possui URL de imagem
            if ((this.imgToolbar != null) && (this.hero.getThumbnail() != null)) {
                // Renderiza a imagem do herói
                Glide.with(this.getApplicationContext())
                        .load(this.hero.getThumbnail().getUrl())
                        .asBitmap()
                        .into(new BitmapImageViewTarget(this.imgToolbar) {
                            @Override
                            public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                                // Executa o método da classe ancestral
                                super.onResourceReady(resource, glideAnimation);
                                // Verifica se o layout da Toolbar foi instanciado
                                if (collapsingToolbarLayout != null) {
                                    // Recupera as cores da imagem de maneira assíncrona
                                    Palette.from(resource)
                                            .generate(new Palette.PaletteAsyncListener() {
                                                @Override
                                                public void onGenerated(Palette palette) {
                                                    // Cria o objeto para receber as cores
                                                    Palette.Swatch swatch = null;
                                                    // Verifica se a cor vibrante foi identificada
                                                    if (palette.getVibrantSwatch() != null) {
                                                        // Atribui o swatch para a variável
                                                        swatch = palette.getVibrantSwatch();
                                                    }
                                                    // Verifica se a cor mais clara foi instanciada
                                                    else if (palette.getLightVibrantSwatch() != null) {
                                                        // Atribui o swatch para a variável
                                                        swatch = palette.getLightVibrantSwatch();
                                                    }
                                                    // Verifica se a swatch de cor dominante foi instanciada
                                                    else if (palette.getDominantSwatch() != null) {
                                                        // Atribui o swatch para a variável
                                                        swatch = palette.getVibrantSwatch();
                                                    }
                                                    // Verifica se a cor mais escura foi instanciada
                                                    else if (palette.getDarkVibrantSwatch() != null) {
                                                        // Atribui o swatch para a variável
                                                        swatch = palette.getDarkVibrantSwatch();
                                                    }
                                                    // Verifica se a variável foi instanciada
                                                    if (swatch != null) {
                                                        collapsingToolbarLayout.setContentScrimColor(swatch.getRgb());
                                                        collapsingToolbarLayout.setStatusBarScrimColor(swatch.getRgb());
                                                        getToolbar().setTitleTextColor(swatch.getTitleTextColor());
                                                    }
                                                }
                                            });
                                }
                            }
                        });
            }
        }
    }
}
