package br.com.andrecampos.marveldatabase.webservice.response;

import br.com.andrecampos.marveldatabase.model.Event;

/**
 * -- MarvelDatabase --
 * Autor: André Campos
 * E-mail: andrecampos.ds@gmail.com
 * Data: 30/09/16 - 12:28
 */

public class EventResponse extends BaseResponse<Event> {
}
