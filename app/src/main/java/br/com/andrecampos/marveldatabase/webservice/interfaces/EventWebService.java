package br.com.andrecampos.marveldatabase.webservice.interfaces;

import java.util.Map;

import br.com.andrecampos.marveldatabase.webservice.response.EventResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;

/**
 * -- MarvelDatabase --
 * Autor: André Campos
 * E-mail: andrecampos.ds@gmail.com
 * Data: 30/09/16 - 12:32
 */

public interface EventWebService {

    @GET("characters/{id}/events")
    Call<EventResponse> list(@Path("id") Long id, @QueryMap Map<String, String> parameters);
}
