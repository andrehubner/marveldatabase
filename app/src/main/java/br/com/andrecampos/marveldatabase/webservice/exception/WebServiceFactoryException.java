package br.com.andrecampos.marveldatabase.webservice.exception;

/**
 * -- MarvelDatabase --
 * Autor: André Campos
 * E-mail: andrecampos.ds@gmail.com
 * Data: 28/09/16 - 19:30
 */

public class WebServiceFactoryException extends RuntimeException {

    /**
     * {@inheritDoc}
     */
    public WebServiceFactoryException(String detailMessage) {
        // Executa o método construtor da classe ancestral
        super(detailMessage);
    }

    /**
     * {@inheritDoc}
     */
    public WebServiceFactoryException(String detailMessage, Throwable cause) {
        // Executa o método construtor da classe ancestral
        super(detailMessage, cause);
    }

    /**
     * {@inheritDoc}
     */
    public WebServiceFactoryException(Throwable cause) {
        // Executa o método construtor da classe ancestral
        super(cause);
    }
}
