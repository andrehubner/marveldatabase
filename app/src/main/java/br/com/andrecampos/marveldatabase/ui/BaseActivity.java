package br.com.andrecampos.marveldatabase.ui;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;

import br.com.andrecampos.marveldatabase.R;

/**
 * -- MarvelDatabase --
 * Autor: André Campos
 * E-mail: andrecampos.ds@gmail.com
 * Data: 29/09/16 - 00:31
 */

public abstract class BaseActivity extends AppCompatActivity {

    private Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        // Executa o método da classe ancestral
        super.onCreate(savedInstanceState);
    }

    @Override
    public void setContentView(View view) {
        // Executa o método da classe ancestral
        super.setContentView(view);
        // Realiza as configurações necessárias da activity
        this.setup();

    }

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        // Executa o método da classe ancestral
        super.setContentView(layoutResID);
        // Realiza as configurações necessárias da activity
        this.setup();
    }

    @Override
    public void setContentView(View view, ViewGroup.LayoutParams params) {
        // Executa o método da classe ancestral
        super.setContentView(view, params);
        // Realiza as configurações necessárias da activity
        this.setup();
    }

    /**
     * Inicializa as variáveis correspondentes às {@link android.view.View views} do layout.
     */
    private void initViews() {
        // Instancia a variável com os componentes definidos no layout da activity
        this.toolbar = (Toolbar) this.findViewById(R.id.toolbar);
    }

    private void setup() {
        // Realiza a inicialização das views
        this.initViews();
        // Realiza as configurações necessárias da Toolbar
        this.setupToolbar();
    }

    public Toolbar getToolbar() {
        // Retorna a instância da toolbar
        return this.toolbar;
    }

    /**
     * Realiza as configurações necessárias na {@link Toolbar}.
     */
    protected void setupToolbar() {
        // Verifica se a toolbar foi instanciada
        if (this.toolbar != null) {
            // Atribui a toolbar como a ActionBar da activity
            this.setSupportActionBar(this.toolbar);
        }
    }
}
