package br.com.andrecampos.marveldatabase.webservice.interfaces;

import java.util.Map;

import br.com.andrecampos.marveldatabase.webservice.response.HeroResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

/**
 * -- MarvelDatabase --
 * Autor: André Campos
 * E-mail: andrecampos.ds@gmail.com
 * Data: 28/09/16 - 19:37
 */

public interface HeroWebService {

    @GET("characters")
    Call<HeroResponse> list(@QueryMap Map<String, String> parameters);

    @GET("characters/{id}")
    Call<HeroResponse> get(@Path("id") Long id, @QueryMap Map<String, String> parameters);

    @GET("characters")
    Call<HeroResponse> findByName(@Query("nameStartsWith") String name,
                                  @QueryMap Map<String, String> parameters);
}
