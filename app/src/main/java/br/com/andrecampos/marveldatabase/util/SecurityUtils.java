package br.com.andrecampos.marveldatabase.util;

import android.text.TextUtils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import br.com.andrecampos.marveldatabase.util.exception.SecurityUtilsException;

/**
 * -- MarvelDatabase --
 * Autor: André Campos
 * E-mail: andrecampos.ds@gmail.com
 * Data: 28/09/16 - 20:02
 */

public class SecurityUtils {

    private static final String MD5_ALGORITHM = "MD5";

    private SecurityUtils() {
        // Construtor privado para impedir que a classe seja instanciada
    }

    public static String toMD5(String text) throws SecurityUtilsException {
        // Verifica se o texto foi informado
        if (!TextUtils.isEmpty(text)) {
            // Define a variável que irá receber o objeto responsável por serializar o texto informado
            MessageDigest digest = null;
            // Inicia um bloco seguro para recuperar a instância do MessageDigest do MD5
            try {
                // Recupera o MessageDigest do MD5 para gerar a hash
                digest = MessageDigest.getInstance(MD5_ALGORITHM);
            } catch (NoSuchAlgorithmException e) {
                // Levanta uma nova exceção no sistema
                throw new SecurityUtilsException("Não foi possível gerar o hash do texto informado, "
                        + "pois o algoritmo MD5 não foi localizado pelo sistema.", e);
            }
            // Verifica se o MessageDigest foi instanciado com sucesso
            if (digest != null) {
                // Insere o array de bytes criado a partir do texto informado no MessageDigest para gerar o hash
                digest.update(text.getBytes());
                // Gera o hash do texto informado em um array de bytes
                byte[] hash = digest.digest();
                // Instancia um novo StringBuilder para gerar o texto do hash
                StringBuilder builder = new StringBuilder();
                // Cria um loop para percorrer todos os dados do array de bytes
                for (byte itemHash : hash) {
                    // Converte o item atual do hash para texto em hexadecimal
                    String hexHash = Integer.toHexString(0xff & itemHash);
                    // Verifica se o hexadecimal possui apenas um caractere
                    if (hexHash.length() == 1) {
                        // Adiciona um zero ao builder antes do hexadecimal
                        builder.append("0");
                    }
                    // Adiciona o texto em hexadecimal ao builder
                    builder.append(hexHash);
                }
                // Retorna o hash gerado pela função
                return builder.toString();
            }
        }
        // Retorna o valor padrão da função
        return null;
    }
}
