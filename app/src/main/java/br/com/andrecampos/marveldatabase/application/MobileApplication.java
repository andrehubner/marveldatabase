package br.com.andrecampos.marveldatabase.application;

import android.app.Application;

import com.orm.SugarContext;

/**
 * -- MarvelDatabase --
 * Autor: André Campos
 * E-mail: andrecampos.ds@gmail.com
 * Data: 29/09/16 - 11:16
 */

public class MobileApplication extends Application {

    @Override
    public void onCreate() {
        // Executa o método da classe ancestral
        super.onCreate();
        // Inicia a biblioteca SugarORM
        SugarContext.init(this.getApplicationContext());
    }

    @Override
    public void onTerminate() {
        // Executa o método da classe ancestral
        super.onTerminate();
        // Finaliza a bibioteca SugarORM
        SugarContext.terminate();
    }
}
