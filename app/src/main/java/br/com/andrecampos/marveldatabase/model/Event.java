package br.com.andrecampos.marveldatabase.model;

import android.os.Parcel;

import com.google.gson.annotations.Expose;
import com.orm.dsl.Table;

import java.util.Date;

import br.com.andrecampos.marveldatabase.util.ParcelUtils;

/**
 * -- MarvelDatabase --
 * Autor: André Campos
 * E-mail: andrecampos.ds@gmail.com
 * Data: 30/09/16 - 01:29
 */
@Table
public class Event extends BaseModel {

    @Expose
    private Long id;
    @Expose
    private String name;
    @Expose
    private String description;
    @Expose
    private Thumbnail thumbnail;
    @Expose
    private Date modified;

    public Event() {
        // Executa o método construtor da classe ancestral
        super();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Thumbnail getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(Thumbnail thumbnail) {
        this.thumbnail = thumbnail;
    }

    public Date getModified() {
        return modified;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }

    protected Event(Parcel source) {
        // Executa o método construtor da classe ancestral
        super(source);
        // Define o valor dos atributos a partir do objeto
        this.id = ParcelUtils.readLong(source.readLong());
        this.name = source.readString();
        this.description = source.readString();
        this.thumbnail = source.readParcelable(Thumbnail.class.getClassLoader());
        this.modified = ParcelUtils.readDate(source.readLong());
    }

    public static final Creator<Event> CREATOR = new Creator<Event>() {
        @Override
        public Event createFromParcel(Parcel parcel) {
            // Retorna uma nova instância de herói
            return new Event(parcel);
        }

        @Override
        public Event[] newArray(int size) {
            // Retorna um array de heróis do tamanho solicitado
            return new Event[size];
        }
    };

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        // Executa o método da classe ancestral
        super.writeToParcel(parcel, flags);
        // Adiciona os atributos na parcela
        parcel.writeLong(ParcelUtils.writeLong(this.id));
        parcel.writeString(this.name);
        parcel.writeString(this.description);
        parcel.writeParcelable(this.thumbnail, flags);
        parcel.writeLong(ParcelUtils.writeDate(this.modified));
    }

    @Override
    public int describeContents() {
        // Retorna um valor únicio para a classe
        return 300;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Event event = (Event) o;

        return id.equals(event.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
