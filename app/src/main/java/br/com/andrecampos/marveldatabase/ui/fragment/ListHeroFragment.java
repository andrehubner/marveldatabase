package br.com.andrecampos.marveldatabase.ui.fragment;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import br.com.andrecampos.marveldatabase.R;
import br.com.andrecampos.marveldatabase.adapter.ListHeroAdapter;
import br.com.andrecampos.marveldatabase.model.Hero;
import br.com.andrecampos.marveldatabase.task.ListHeroTask;
import br.com.andrecampos.marveldatabase.ui.DetailHeroActivity;
import br.com.andrecampos.marveldatabase.webservice.response.HeroResponse;

/**
 * -- MarvelDatabase --
 * Autor: André Campos
 * E-mail: andrecampos.ds@gmail.com
 * Data: 29/09/16 - 00:35
 */

public class ListHeroFragment extends BaseFragment implements ListHeroAdapter.OnClickListener,
        ListHeroTask.OnFinishedListener, ListHeroTask.OnPostExecuteListener<HeroResponse>,
        SwipeRefreshLayout.OnRefreshListener {

    private SwipeRefreshLayout swipeLayout;
    private RecyclerView recyclerView;
    private ListHeroAdapter adapter;
    private ListHeroTask task;

    private String filter;
    private int offset;
    private boolean allDataLoaded;
    private boolean clearData;

    public ListHeroFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        // Executa o método da classe ancestral
        super.onCreate(savedInstanceState);
        // Informa que o fragmento atual possui menu a ser adicionado na ActionBar
        this.setHasOptionsMenu(true);
        // Instancia um novo objeto do adapter
        this.adapter = new ListHeroAdapter(this.getActivity().getApplicationContext(),
                null,
                this);
        // Define o valor inicial das variáveis
        this.offset = 0;
        this.allDataLoaded = false;
        this.clearData = false;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Renderiza o layout do fragmento
        final View layout = inflater.inflate(R.layout.fragment_list_hero, container, false);
        // Realiza a inicialização das views
        this.initViews(layout);
        // Realiza as configurações do RecyclerView
        this.setupRecyclerView();
        // Realiza as configurações do SwipeLayout
        this.setupSwipeLayout();
        // Retorna o layout do fragmento
        return layout;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        syncHero(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // Executa o método da classe ancestral
        super.onCreateOptionsMenu(menu, inflater);
        // Renderiza o menu configurado para o fragmento
        inflater.inflate(R.menu.menu_list_hero, menu);
        // Recupera o item do menu referente à pesquisa
        MenuItem item = menu.findItem(R.id.action_search);
        // Recupera o SearchView configurado
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        // Verifica se o searchview foi instanciado
        if (searchView != null) {
            // Atribui o evento de search ao SearchView
            searchView.setOnQueryTextListener(this.createOnSearchViewListener());
            // Atribui o evento ao fechar o SearchView
            searchView.setOnCloseListener(this.createOnCloseSearchViewListener());
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Recupera o ID da opção selecionada
        final int id = item.getItemId();
        // Verifica o ID do menu selecionado
        switch (id) {
            case R.id.action_order:
                // Verifica se o adapter foi instanciado
                if (this.adapter != null) {
                    // Realiza a ordenação da lista do adapter
                    this.adapter.sort();
                }
                // Encerra o comando switch
                break;
        }
        // Retorna o valor da classe ancestral
        return super.onOptionsItemSelected(item);
    }

    /**
     * Inicializa as variáveis correspondentes às {@link android.view.View views} do layout.
     */
    private void initViews(@NonNull View layout) {
        // Instancia a variável com os componentes definidos no layout do fragmento
        this.recyclerView = (RecyclerView) layout.findViewById(R.id.recyclerView);
        this.swipeLayout = (SwipeRefreshLayout) layout.findViewById(R.id.swipeLayout);
    }

    /**
     * Realiza as configurações do {@link RecyclerView}.
     */
    private void setupRecyclerView() {
        // Verifica se o recyclerView foi instanciado
        if (this.recyclerView != null) {
            // Instancia o gerenciador de layout do RecyclerView
            LinearLayoutManager layoutManager = new GridLayoutManager(this.getContext(), 2);
            // Adiciona o gerenciador de layouts ao RecyclerView
            this.recyclerView.setLayoutManager(layoutManager);
            // Configura o listener de scroll do recyclerview
            this.recyclerView.addOnScrollListener(this.createRecyclerViewScrollListener());
            // Atribui o adapter do RecyclerView
            this.recyclerView.setAdapter(this.adapter);
        }
    }

    /**
     * Realiza as configurações do {@link SwipeRefreshLayout}.
     */
    private void setupSwipeLayout() {
        // Verifica se o SwipeLayout foi instanciado
        if (this.swipeLayout != null) {
            // Define as cores do ícone de atualização
            this.swipeLayout.setColorSchemeResources(R.color.colorPrimary,
                    R.color.colorAccent,
                    android.R.color.holo_blue_light,
                    android.R.color.holo_green_light);
            // Atribui o listener de refresh do layout
            this.swipeLayout.setOnRefreshListener(this);
        }
    }

    /**
     * Cria o objeto responsável por acompanhar as mudanças realizadas no scroll do {@link RecyclerView}.
     * @return Um novo {@link android.support.v7.widget.RecyclerView.OnScrollListener}.
     */
    private RecyclerView.OnScrollListener createRecyclerViewScrollListener() {
        // Retorna uma nova instância do OnScrollListener
        return new RecyclerView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                // Executa o método da classe ancestral
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                // Executa o método da classe ancestral
                super.onScrolled(recyclerView, dx, dy);
                // Verifica se o scroll está sendo realizado para baixo
                if (dy > 0) {
                    // Verifica se o layoutManager do RecyclerView é uma instância de GridLayoutManager
                    if (recyclerView.getLayoutManager() instanceof GridLayoutManager) {
                        // Realiza um cast do LayoutManager
                        GridLayoutManager manager = (GridLayoutManager) recyclerView.getLayoutManager();
                        // Calcula o valor máximo que o limite deve possuir para carregar mais dados
                        int limit = Math.max(manager.getItemCount() - (manager.getChildCount() * 3), 0);
                        // Verifica se o limite foi excedido e se o limite é maior que zero
                        if ((limit <= manager.findFirstVisibleItemPosition()) && (limit > 0)) {
                            // Atualiza o valor do offset com o valor de registros no Recyclerview
                            offset = manager.getItemCount();
                            // Incrementa a lista de heróis
                            syncHero(false);
                        }
                        // Verifica se a lista atingiu o final
                        else if ((manager.getItemCount() - manager.getChildCount()) == manager.findFirstVisibleItemPosition()) {
                            // Atualiza a lista de heróis
                            syncHero(true);
                        }
                    }
                }
            }
        };
    }

    private void syncHero(boolean showProgress) {
        // Verifica se o objeto de task é nulo ou não está em execução e se existe pendência de dados
        if (((this.task == null) || (this.task.getStatus() != AsyncTask.Status.RUNNING)) && (!this.allDataLoaded)) {
            // Instancia uma nova task atribuindo o offset atual
            this.task = new ListHeroTask(this.getActivity().getApplicationContext(),
                    this.filter,
                    this.offset);
            // Atribui o listener que será executado no encerramento do método
            this.task.setOnFinishedListener(this);
            // Atribui o listener que será executado quando a rotina for realizada com sucesso
            this.task.setOnPostExecuteListener(this);
            // Inicia a execução da task
            this.task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
        // Verifica se o SwipeLayout foi instanciado
        if (this.swipeLayout != null) {
            // Verifica se foi solicitado a exibição do progresso
            if ((showProgress) && (this.task != null) && (this.task.getStatus() == AsyncTask.Status.RUNNING)) {
                // Ativa o ícone de progresso
                this.swipeLayout.setRefreshing(true);
            }
        }
    }

    @Override
    public void onItemClick(Hero hero) {
        // Verifica se foi informado um herói
        if (hero != null) {
            // Cria uma intent para chamar a tela de detalhe
            Intent intent = new Intent(this.getContext(), DetailHeroActivity.class);
            // Atribui o herói informado à intent
            intent.putExtra(DetailHeroActivity.EXTRA_HERO, (Parcelable) hero);
            // Chama a nova activity
            this.startActivity(intent);
        }
    }

    @Override
    public void onFinished() {
        // Verifica se SwipeLayout foi instanciado
        if (this.swipeLayout != null) {
            // Desativa o ícone de refresh do layout
            this.swipeLayout.setRefreshing(false);
        }
    }

    @Override
    public void onPostExecute(HeroResponse result) {
        // Verifica se o adapter foi instanciado
        if (this.adapter != null) {
            // Adiciona o conteúdo recuperado na listagem de heróis
            this.adapter.addHeroes(result.getResults());
            // Atualiza se todos os dados foram carregados
            this.allDataLoaded = result.isEof();
        }
    }

    private SearchView.OnQueryTextListener createOnSearchViewListener() {
        // Retorna uma nova instância do OnQueryTextListener para realizar as buscas
        return new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // Atualiza o texto do filtro
                filter = query;
                // Zera o valor do offset
                offset = 0;
                // Define que os dados não foram carregados
                allDataLoaded = false;
                // Verifica se o adapter foi instanciado
                if (adapter != null) {
                    // Limpa todos os heróis listados
                    adapter.setHeroes(null);
                }
                // Inicia a busca de heróis
                syncHero(true);
                // Retorna o valor da função
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        };
    }

    private SearchView.OnCloseListener createOnCloseSearchViewListener() {
        // Retorna uma nova instância do listener
        return new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                // Define que a lista deve ser limpa
                clearData = true;
                // Limpa o valor do filter
                filter = null;
                // Retorna o valor da função
                return false;
            }
        };
    }

    @Override
    public void onRefresh() {
        // Verifica se a lista deve ser limpa
        if (this.clearData) {
            // Informa que a lista não deve ser limpa
            this.clearData = false;
            // Verifica se o adapter foi instanciado
            if (this.adapter != null) {
                // Limpa a lista de heróis
                this.adapter.setHeroes(null);
            }
        }
        // Informa que os dados não foram carregados
        this.allDataLoaded = false;
        // Realiza a sincronização dos heróis
        this.syncHero(false);
    }
}
