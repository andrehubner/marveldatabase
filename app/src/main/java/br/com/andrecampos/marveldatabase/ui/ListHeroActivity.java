package br.com.andrecampos.marveldatabase.ui;

import android.os.Bundle;

import br.com.andrecampos.marveldatabase.R;

/**
 * -- MarvelDatabase --
 * Autor: André Campos
 * E-mail: andrecampos.ds@gmail.com
 * Data: 29/09/16 - 00:35
 */

public class ListHeroActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_hero);
    }

}
