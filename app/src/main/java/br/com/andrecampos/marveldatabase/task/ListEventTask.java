package br.com.andrecampos.marveldatabase.task;

import android.content.Context;

import br.com.andrecampos.marveldatabase.webservice.WebServiceFactory;
import br.com.andrecampos.marveldatabase.webservice.interfaces.EventWebService;
import br.com.andrecampos.marveldatabase.webservice.response.EventResponse;
import retrofit2.Response;

/**
 * -- MarvelDatabase --
 * Autor: André Campos
 * E-mail: andrecampos.ds@gmail.com
 * Data: 30/09/16 - 12:23
 */

public class ListEventTask extends BaseTask<EventResponse> {

    private Long heroId;
    private int offset;

    public ListEventTask(Context context, Long heroId, int offset) {
        // Executa o método construtor da classe ancestral
        super(context);
        // Define o valor dos atributos
        this.heroId = heroId;
        this.offset = offset;
    }

    @Override
    protected EventResponse doInBackground(Void... voids) {
        // Instancia o objeto responsável por realizar a comunicação com o webservice
        EventWebService webService = WebServiceFactory.build(EventWebService.class);
        // Define a variável que irá receber o retorno do webservice
        Response<EventResponse> response;
        // Inicia um bloco seguro para realizar a chamada
        try {
            // Executa a chamada do webservice
            response = webService.list(this.heroId,
                    WebServiceFactory.createWebServiceParameters(this.getContext(), this.offset))
                    .execute();
        } catch (Exception e) {
            // Imprime a pilha de exceções
            e.printStackTrace();
            // Retorna nulo, indicando que a função não foi realizada com sucesso
            return null;
        }
        // Verifica se ocorreu algum erro durante a execução do método
        if (!response.isSuccessful()) {
            // Retorna nulo, indicando que a função não foi realizada com sucesso
            return null;
        }
        // Retorna o objeto de response que possui os dados dos eventos
        return response.body();
    }
}
