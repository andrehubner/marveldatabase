package br.com.andrecampos.marveldatabase.comparator;

import java.util.Comparator;

import br.com.andrecampos.marveldatabase.model.Hero;

/**
 * -- MarvelDatabase --
 * Autor: André Campos
 * E-mail: andrecampos.ds@gmail.com
 * Data: 30/09/16 - 11:57
 */

public class HeroCompataror implements Comparator<Hero> {

    @Override
    public int compare(Hero hero1, Hero hero2) {
        // Retorna o resultado da comparação entre os dois registros
        return hero1.compareTo(hero2);
    }
}
