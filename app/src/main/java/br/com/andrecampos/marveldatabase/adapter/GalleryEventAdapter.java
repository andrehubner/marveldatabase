package br.com.andrecampos.marveldatabase.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.List;

import br.com.andrecampos.marveldatabase.R;
import br.com.andrecampos.marveldatabase.model.Event;

/**
 * -- MarvelDatabase --
 * Autor: André Campos
 * E-mail: andrecampos.ds@gmail.com
 * Data: 30/09/16 - 12:54
 */

public class GalleryEventAdapter extends RecyclerView.Adapter<GalleryEventAdapter.GalleryEventViewHolder> {

    private Context context;
    private List<Event> events;

    public GalleryEventAdapter(@NonNull Context context, List<Event> events) {
        // Executa o método construtor da classe ancestral
        super();
        // Define os atributos do objeto
        this.context = context;
        this.events = events;
    }

    public void addEvents(List<Event> newEvents) {
        // Verifica se a lista atual está nula
        if (this.events == null) {
            // Executa o método de definir eventos
            this.setHeroes(newEvents);
        }
        else {
            // Verifica se a lista de novos eventos foi informada
            if (newEvents != null) {
                // Recupera o índice do primeiro registro adicionado
                int startIndex = this.events.size();
                // Adiciona os registros informados à lista de eventos existentes
                this.events.addAll(newEvents);
                // Notifica que novos itens foram inseridos
                this.notifyItemRangeInserted(startIndex, newEvents.size());
            }
        }
    }

    public void setHeroes(List<Event> heroes) {
        // Atribui a lista de eventos ao adapter
        this.events = heroes;
        // Notifica que houve alteração nos dados
        this.notifyDataSetChanged();
    }

    @Override
    public GalleryEventAdapter.GalleryEventViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // Renderiza o layout do item
        View layout = LayoutInflater.from(context)
                .inflate(R.layout.item_list_event, parent, false);
        // Retorna um novo ViewHolder para o layout
        return new GalleryEventViewHolder(layout);
    }

    @Override
    public void onBindViewHolder(GalleryEventAdapter.GalleryEventViewHolder holder, int position) {
        // Recupera o item correspondente à posição informada
        Event event = this.getItem(position);
        // Verifica se o item foi recuperado com sucesso
        if (event != null) {
            // Carrega a imagem do herói
            Glide.with(this.context)
                    .load(event.getThumbnail().getUrl())
                    .into(holder.imgEvent);
        }
        else {
            // Limpa todos os atributos do item
            holder.imgEvent.setImageBitmap(null);
        }
    }

    @Override
    public int getItemCount() {
        // Retorna a quantidade de itens existentes no adapter
        return this.events != null ? this.events.size() : 0;
    }

    /**
     * Retorna o {@link Event Evento} correspondente à posição informada.
     * @param position Índice do {@link Event evento} na lista.
     * @return Caso o índice seja válido, será retornado um {@link Event evento}, do contrário retornará nulo.
     */
    private Event getItem(int position) {
        // Verifica se a lista de heróis foi instanciada
        if (this.events != null) {
            // Verifica se a posição informada é válida
            if ((position >= 0) && (position < this.events.size())) {
                // Retorna o item da posição informada
                return this.events.get(position);
            }
        }
        // Retorna nulo, indicando que o item não foi encontrado
        return null;
    }

    class GalleryEventViewHolder extends RecyclerView.ViewHolder {

        ImageView imgEvent;

        public GalleryEventViewHolder(View itemView) {
            // Executa o método construtor da classe ancestral
            super(itemView);
            // Inicializa as variáveis correspondentes ao layout da célula
            this.initViews(itemView);
        }

        private void initViews(View layout) {
            // Inicializa as variáveis correspondentes ao layout da célula
            this.imgEvent = (ImageView) layout.findViewById(R.id.imgEvent);
        }
    }
}
