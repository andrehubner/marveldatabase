package br.com.andrecampos.marveldatabase.webservice.response;

import com.google.gson.annotations.Expose;

import java.util.List;

import br.com.andrecampos.marveldatabase.model.BaseModel;

/**
 * -- MarvelDatabase --
 * Autor: André Campos
 * E-mail: andrecampos.ds@gmail.com
 * Data: 28/09/16 - 20:49
 */

public abstract class BaseResponse<T extends BaseModel> {

    @Expose
    private int code;
    @Expose
    private String status;
    @Expose
    private BaseData<T> data;

    public int getCode() {
        return code;
    }

    public String getStatus() {
        return status;
    }

    public BaseData<T> getData() {
        return data;
    }

    public List<T> getResults() {
        // Verifica se o objeto de data foi informado
        if (this.getData() != null) {
            // Retorna a lista de objetos
            return this.getData().getResults();
        }
        // Retorna o valor da função
        return null;
    }

    public boolean isEof() {
        // Verifica se o objeto de data foi informado
        if (this.getData() != null) {
            // Retorna se todos os dados foram carregados
            return this.getData().isEof();
        }
        // Retorna o valor da função
        return true;
    }
}
