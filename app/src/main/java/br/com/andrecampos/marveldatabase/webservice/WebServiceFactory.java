package br.com.andrecampos.marveldatabase.webservice;

import android.content.Context;
import android.support.annotation.NonNull;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import br.com.andrecampos.marveldatabase.R;
import br.com.andrecampos.marveldatabase.gson.GsonFactory;
import br.com.andrecampos.marveldatabase.util.SecurityUtils;
import br.com.andrecampos.marveldatabase.util.exception.SecurityUtilsException;
import br.com.andrecampos.marveldatabase.webservice.exception.WebServiceFactoryException;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * -- MarvelDatabase --
 * Autor: André Campos
 * E-mail: andrecampos.ds@gmail.com
 * Data: 28/09/16 - 19:23
 */

public class WebServiceFactory {

    private static final String BASE_URL = "http://gateway.marvel.com/v1/public/";

    private static final String QUERY_API_KEY = "apikey";
    private static final String QUERY_TIMESTAMP = "ts";
    private static final String QUERY_HASH = "hash";
    private static final String QUERY_OFFSET = "offset";
    private static final String QUERY_LIMIT = "limit";

    private static final int SECONDS_TIME_OUT = 60;

    private static Retrofit retrofit;

    private WebServiceFactory() {
        // Construtor privado para impedir que a classe seja instanciada
    }

    /**
     * Recupera a instância do objeto Retrofit para criação de conexões com webservices.
     * @return Instância do objeto "Retrofit".
     */
    private static Retrofit getRetrofit() {
        // Verifica se a instancia do retrofit foi iniciada
        if (retrofit == null) {
            // Instancia um novo objeto do retrofit
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(
                            GsonFactory.build()))
                    .client(createClient())
                    .build();
        }
        // Retorna o objeto criado
        return retrofit;
    }

    /**
     * Instancia um novo objeto {@link OkHttpClient} com o timeout ajustado de acordo
     * com o parâmetro configurado na classe {@link WebServiceFactory}.
     * @return Instância do objeto {@link OkHttpClient} configurado.
     */
    private static OkHttpClient createClient() {
        // Retorna um novo client para realização das requisições HTTP
        return new OkHttpClient.Builder()
                .connectTimeout(SECONDS_TIME_OUT, TimeUnit.SECONDS)
                .readTimeout(SECONDS_TIME_OUT, TimeUnit.SECONDS)
                .writeTimeout(SECONDS_TIME_OUT * 2, TimeUnit.SECONDS)
                .build();
    }

    /**
     * Contrói o objeto de acesso ao webservice de acordo com a classe da interface informada.
     * @param clazz Classe da interface que será utilizada para construir o objeto de comunicação.
     * @return Objeto de comunicação com o webservice.
     * @throws WebServiceFactoryException Caso a classe da interface não tenha sido informada.
     */
    public static <T> T build(Class<T> clazz) {
        // Verifica se a classe não foi informada
        if (clazz == null) {
            // Levanta uma nova exceção em tempo de execução
            throw new WebServiceFactoryException("Não foi possível construir o objeto de comunicação webservice, "
                    + "pois a classe da interface modelo não foi informada.");
        }
        // Realiza o build a partir da instância do Retrofit e retorna o objeto criado
        return getRetrofit().create(clazz);
    }

    /**
     * Constrói um {@link Map} contendo os parâmetros que devem ser enviados para o webservice da Marvel.
     * @param context Contexto para acesso aos recursos da aplicação.
     * @return Instância do objeto {@link Map} contendo os parâmetros para acesso ao webservice da Marvel.
     * @throws WebServiceFactoryException Caso ocorra alguma falha ao gerar o {@link Map} com os parâmetros.
     */
    public static Map<String, String> createWebServiceParameters(@NonNull Context context)
            throws WebServiceFactoryException {
        // Executa a chamada do método passando como offset padrão o valor zero.
        return createWebServiceParameters(context, 0);
    }

    /**
     * Constrói um {@link Map} contendo os parâmetros que devem ser enviados para o webservice da Marvel.
     * @param context Contexto para acesso aos recursos da aplicação.
     * @param offset Valor inicial para paginação dos dados.
     * @return Instância do objeto {@link Map} contendo os parâmetros para acesso ao webservice da Marvel.
     * @throws WebServiceFactoryException Caso ocorra alguma falha ao gerar o {@link Map} com os parâmetros.
     */
    public static Map<String, String> createWebServiceParameters(@NonNull Context context, int offset)
            throws WebServiceFactoryException {
        // Instancia um novo Map para receber os parâmetros que devem ser enviados ao WebService
        Map<String, String> parameters = new HashMap<String, String>();
        // Gera o timestamp com base no data atual do dispositivo
        final String timestamp = String.valueOf(System.currentTimeMillis());
        // Adiciona os parâmetros no Map
        parameters.put(QUERY_API_KEY, context.getString(R.string.marvel_public_key));
        parameters.put(QUERY_TIMESTAMP, timestamp);
        parameters.put(QUERY_LIMIT, String.valueOf(context.getResources()
                .getInteger(R.integer.query_limit)));
        parameters.put(QUERY_OFFSET, String.valueOf(offset));
        // Inicia um bloco seguro para criar o hash
        try {
            // Adiciona o hash aos parâmetros
            parameters.put(QUERY_HASH, createHash(context, timestamp));
        } catch (SecurityUtilsException e) {
            // Levanta uma nova exceção
            throw new WebServiceFactoryException(e);
        }
        // Retorna o objeto configurado
        return parameters;
    }

    /**
     * Gera a hash MD5 com base da documentação da Marvel para acesso ao webservice.
     * @param context Contexto para acesso aos recursos da aplicação.
     * @param timestamp Texto do timestamp que será adicionado ao hash.
     * @return {@link String} contendo o hash MD5 para acesso ao webservice da Marvel.
     * @throws SecurityUtilsException Caso ocorra alguma exceção durante a conversão do hash para MD5.
     */
    private static String createHash(@NonNull Context context, @NonNull String timestamp) throws SecurityUtilsException {
        // Instancia um novo StringBuilder para montar o hash
        String hash = timestamp +
                context.getString(R.string.marvel_private_key) +
                context.getString(R.string.marvel_public_key);
        // Retorna o hash MD5 de acordo com a documentação da Marvel
        return SecurityUtils.toMD5(hash);
    }
}
