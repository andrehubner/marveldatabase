package br.com.andrecampos.marveldatabase.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.orm.SugarRecord;

import java.io.Serializable;

/**
 * -- MarvelDatabase --
 * Autor: André Campos
 * E-mail: andrecampos.ds@gmail.com
 * Data: 29/09/16 - 15:49
 */

public abstract class BaseModel extends SugarRecord implements Serializable, Parcelable {

    public BaseModel() {
        // Executa o método da classe ancestral
        super();
    }

    public BaseModel(Parcel source) {
        // Executa o método construtor padrão
        this();
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {

    }
}
