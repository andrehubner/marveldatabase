package br.com.andrecampos.marveldatabase.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.Collections;
import java.util.List;

import br.com.andrecampos.marveldatabase.R;
import br.com.andrecampos.marveldatabase.comparator.HeroCompataror;
import br.com.andrecampos.marveldatabase.model.Hero;

/**
 * -- MarvelDatabase --
 * Autor: André Campos
 * E-mail: andrecampos.ds@gmail.com
 * Data: 29/09/16 - 01:11
 */

public class ListHeroAdapter extends RecyclerView.Adapter<ListHeroAdapter.ListHeroViewHolder> {

    private Context context;
    private List<Hero> heroes;
    private OnClickListener onClickListener;

    public ListHeroAdapter(@NonNull Context context, List<Hero> heroes, OnClickListener onClickListener) {
        // Executa o método construtor da classe ancestral
        super();
        // Define os atributos do objeto
        this.context = context;
        this.heroes = heroes;
        this.onClickListener = onClickListener;
    }

    public void addHeroes(List<Hero> newHeroes) {
        // Verifica se a lista atual está nula
        if (this.heroes == null) {
            // Executa o método de definir heróis
            this.setHeroes(newHeroes);
        }
        else {
            // Verifica se a lista de novos heróis foi informada
            if (newHeroes != null) {
                // Recupera o índice do primeiro registro adicionado
                int startIndex = this.heroes.size();
                // Adiciona os registros informados à lista de heróis existentes
                this.heroes.addAll(newHeroes);
                // Notifica que novos itens foram inseridos
                this.notifyItemRangeInserted(startIndex, newHeroes.size());
            }
        }
    }

    public void setHeroes(List<Hero> heroes) {
        // Atribui a lista de heróis ao adapter
        this.heroes = heroes;
        // Verifica se existem dados informados
        if (this.heroes != null) {
            // Realiza a ordenação da lista
            this.sort();
        }
        // Notifica que houve alteração nos dados
        this.notifyDataSetChanged();
    }

    @Override
    public ListHeroViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // Renderiza o layout do item
        View layout = LayoutInflater.from(context)
                .inflate(R.layout.item_list_hero, parent, false);
        // Instancia um novo ViewHolder para o layout
        final ListHeroViewHolder holder = new ListHeroViewHolder(layout);
        // Define o evento de click do item
        holder.itemView.setOnClickListener(this.createOnClickListener(holder));
        // Retorna o ViewHolder do objeto
        return holder;
    }

    @Override
    public void onBindViewHolder(final ListHeroViewHolder holder, int position) {
        // Recupera o item correspondente à posição informada
        Hero hero = this.getItem(position);
        // Verifica se o item foi recuperado com sucesso
        if (hero != null) {
            // Carrega a imagem do herói
            Glide.with(this.context)
                    .load(hero.getThumbnail().getUrl())
                    .into(holder.imgBackground);
            // Atribui o nome do herói
            holder.txtName.setText(hero.getName());
            // Define o ícone do botão
            holder.btnSave.setImageResource(hero.isSaved()
                    ? R.drawable.ic_favorite_black_24dp
                    : R.drawable.ic_favorite_border_black_24dp);
            // Define a cor do ícone do botão
            holder.btnSave.setColorFilter(ContextCompat.getColor(this.context, hero.isSaved()
                    ? R.color.colorSavedButton
                    : android.R.color.white));
        }
        else {
            // Limpa todos os atributos do item
            holder.imgBackground.setImageBitmap(null);
            holder.txtName.setText("");
            holder.btnSave.setImageResource(R.drawable.ic_favorite_border_black_24dp);
        }
    }

    @Override
    public int getItemCount() {
        // Retorna a quantidade de itens existentes no adapter
        return this.heroes != null ? this.heroes.size() : 0;
    }

    /**
     * Retorna o {@link Hero} correspondente à posição informada.
     * @param position Índice do {@link Hero} na lista.
     * @return Caso o índice seja válido, será retornado um {@link Hero}, do contrário retornará nulo.
     */
    private Hero getItem(int position) {
        // Verifica se a lista de heróis foi instanciada
        if (this.heroes != null) {
            // Verifica se a posição informada é válida
            if ((position >= 0) && (position < this.heroes.size())) {
                // Retorna o item da posição informada
                return this.heroes.get(position);
            }
        }
        // Retorna nulo, indicando que o item não foi encontrado
        return null;
    }

    /**
     * Instancia um novo listener para informar quando um item da lista for selecionado.
     * @param holder {@link ListHeroViewHolder ViewHolder} correspondente ao item selecionado.
     * @return Um novo objeto da classe {@link OnClickListener}.
     */
    private View.OnClickListener createOnClickListener(final ListHeroViewHolder holder) {
        // Retorna uma nova instância do listener de click
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Verifica se o listener foi instanciado
                if (onClickListener != null) {
                    // Recupera a posição do item selecionado
                    final int position = holder.getAdapterPosition();
                    // Recupera o item da posição informada
                    Hero hero = getItem(position);
                    // Executa a função de click informando o herói selecionado
                    onClickListener.onItemClick(hero);
                }
            }
        };
    }

    /**
     * Realiza a ordenação da lista de heróis.
     */
    public void sort() {
        // Instancia um novo Comparator de herói
        HeroCompataror compataror = new HeroCompataror();
        // Realiza a ordenação do array
        Collections.sort(this.heroes, compataror);
        // Informa que os dados foram atualizados
        this.notifyDataSetChanged();
    }

    class ListHeroViewHolder extends RecyclerView.ViewHolder {

        ImageView imgBackground;
        TextView txtName;
        ImageButton btnSave;

        public ListHeroViewHolder(View itemView) {
            // Executa o método construtor da classe ancestral
            super(itemView);
            // Inicializa as variáveis correspondentes ao layout da célula
            this.initViews(itemView);
            // Realiza as configurações para o funcionamento do click do botão de salvar
            this.setupBtnSave();
        }

        private void initViews(View layout) {
            // Instancia a variável com os componentes definidos no layout do item
            this.imgBackground = (ImageView) layout.findViewById(R.id.imgBackground);
            this.txtName = (TextView) layout.findViewById(R.id.txtName);
            this.btnSave = (ImageButton) layout.findViewById(R.id.btnSave);
        }

        private void setupBtnSave() {
            // Verifica se o botão de salvar foi instanciado
            if (this.btnSave != null) {
                // Configura o evento de click do botão
                this.btnSave.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        // Recupera a posição do item selecionado
                        final int position = getAdapterPosition();
                        // Recupera o item da posição selecionada
                        Hero hero = getItem(position);
                        // Verifica se foi localiado algum registro
                        if (hero != null) {
                            // Inverte o valor do registro
                            hero.setSaved(!hero.isSaved());
                            // Notifica que o registro foi atualizado
                            notifyItemChanged(position);
                        }
                    }
                });
            }
        }
    }

    public interface OnClickListener {
        void onItemClick(Hero hero);
    }
}
