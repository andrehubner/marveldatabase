package br.com.andrecampos.marveldatabase.model;

import android.os.Parcel;
import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.orm.dsl.Table;

import java.util.Date;

import br.com.andrecampos.marveldatabase.util.ParcelUtils;

/**
 * -- MarvelDatabase --
 * Autor: André Campos
 * E-mail: andrecampos.ds@gmail.com
 * Data: 28/09/16 - 20:34
 */

@Table
public class Hero extends BaseModel implements Comparable<Hero> {

    @Expose
    private Long id;
    @Expose
    private String name;
    @Expose
    private String description;
    @Expose
    private Thumbnail thumbnail;
    private boolean saved;
    @Expose
    private Date modified;

    public Hero() {
        // Executa o método construtor da classe ancestral
        super();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Thumbnail getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(Thumbnail thumbnail) {
        this.thumbnail = thumbnail;
    }

    public boolean isSaved() {
        return saved;
    }

    public void setSaved(boolean saved) {
        // Define o valor do atributo
        this.saved = saved;
        // Verifica se o registro foi salvo
        if (this.saved) {
            // Salva o registro no banco
            this.save();
        } else {
            // Exclui o registro
            this.delete();
        }
    }

    public Date getModified() {
        return modified;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }

    protected Hero(Parcel source) {
        // Executa o método construtor da classe ancestral
        super(source);
        // Define o valor dos atributos a partir do objeto
        this.id = ParcelUtils.readLong(source.readLong());
        this.name = source.readString();
        this.description = source.readString();
        this.thumbnail = source.readParcelable(Thumbnail.class.getClassLoader());
        this.saved = ParcelUtils.readBoolean(source.readInt());
        this.modified = ParcelUtils.readDate(source.readLong());
    }

    public static final Creator<Hero> CREATOR = new Creator<Hero>() {
        @Override
        public Hero createFromParcel(Parcel parcel) {
            // Retorna uma nova instância de herói
            return new Hero(parcel);
        }

        @Override
        public Hero[] newArray(int size) {
            // Retorna um array de heróis do tamanho solicitado
            return new Hero[size];
        }
    };

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        // Executa o método da classe ancestral
        super.writeToParcel(parcel, flags);
        // Adiciona os atributos na parcela
        parcel.writeLong(ParcelUtils.writeLong(this.id));
        parcel.writeString(this.name);
        parcel.writeString(this.description);
        parcel.writeParcelable(this.thumbnail, flags);
        parcel.writeInt(ParcelUtils.writeBoolean(this.saved));
        parcel.writeLong(ParcelUtils.writeDate(this.modified));
    }

    @Override
    public int describeContents() {
        // Retorna um valor únicio para a classe
        return 100;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Hero hero = (Hero) o;

        return id.equals(hero.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public int compareTo(@NonNull Hero hero) {
        // Verifica se o outro herói foi instanciado
        if (hero == null) {
            // Retorna o valor da função
            return -1;
        }
        // Verifica se o registro atual está favoritado e o outro não
        if (this.isSaved() && !hero.isSaved()) {
            // Retorna o valor da função
            return -1;
        }
        // Verifica se o registro atual não está favoritado e o outro está
        if (!this.isSaved() && hero.isSaved()) {
            // Retorna o valor da função
            return 1;
        }
        // Verifica se os dois nomes são nulos
        if ((this.getName() == null) && (hero.getName() == null)) {
            // Retorna o valor da função
            return 0;
        }
        // Verifica se o nome do herói está nulo
        if (this.getName() == null) {
            // Retorna o valor da função
            return 1;
        }
        // Verifica se o nome do outro herói está nulo
        if (hero.getName() == null) {
            // Retorna o valor da função
            return -1;
        }
        // Retorna a ordenação com base no nome do herói
        return this.getName().compareTo(hero.getName());
    }
}
