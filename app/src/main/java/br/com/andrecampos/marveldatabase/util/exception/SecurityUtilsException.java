package br.com.andrecampos.marveldatabase.util.exception;

/**
 * -- MarvelDatabase --
 * Autor: André Campos
 * E-mail: andrecampos.ds@gmail.com
 * Data: 28/09/16 - 20:11
 */

public class SecurityUtilsException extends Exception {

    /**
     * {@inheritDoc}
     */
    public SecurityUtilsException(String detailMessage) {
        // Executa o método construtor da classe ancestral
        super(detailMessage);
    }

    /**
     * {@inheritDoc}
     */
    public SecurityUtilsException(String detailMessage, Throwable throwable) {
        // Executa o método construtor da classe ancestral
        super(detailMessage, throwable);
    }
}
