package br.com.andrecampos.marveldatabase.webservice.response;

import br.com.andrecampos.marveldatabase.model.Hero;

/**
 * -- MarvelDatabase --
 * Autor: André Campos
 * E-mail: andrecampos.ds@gmail.com
 * Data: 28/09/16 - 21:00
 */

public class HeroResponse extends BaseResponse<Hero> {
}
