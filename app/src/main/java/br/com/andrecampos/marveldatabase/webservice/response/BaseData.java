package br.com.andrecampos.marveldatabase.webservice.response;

import com.google.gson.annotations.Expose;

import java.util.List;

import br.com.andrecampos.marveldatabase.model.BaseModel;

/**
 * -- MarvelDatabase --
 * Autor: André Campos
 * E-mail: andrecampos.ds@gmail.com
 * Data: 28/09/16 - 23:19
 */

public class BaseData<T extends BaseModel> {

    @Expose
    private int offset;
    @Expose
    private int limit;
    @Expose
    private int total;
    @Expose
    private int count;
    @Expose
    private List<T> results;

    public int getOffset() {
        return offset;
    }

    public int getLimit() {
        return limit;
    }

    public int getTotal() {
        return total;
    }

    public int getCount() {
        return count;
    }

    public List<T> getResults() {
        return results;
    }

    public boolean isEof() {
        return (this.getOffset() + this.getCount()) >= this.getTotal();
    }
}
