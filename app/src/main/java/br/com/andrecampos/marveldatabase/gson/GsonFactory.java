package br.com.andrecampos.marveldatabase.gson;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * -- MarvelDatabase --
 * Autor: André Campos
 * E-mail: andrecampos.ds@gmail.com
 * Data: 28/09/16 - 19:26
 */

public class GsonFactory {

    private GsonFactory() {
        // Construtor privado para impedir que a classe seja instanciada
    }

    /**
     * Retorna uma nova instância de um {@link Gson} para ser utilizado na serialização e desserialização
     * de objetos e JSON.
     * @return Nova instância de {@link Gson}.
     */
    public static Gson build() {
        // Retorna uma nova instância de um Gson
        return new GsonBuilder()
                .excludeFieldsWithoutExposeAnnotation()
                .create();
    }
}
