package br.com.andrecampos.marveldatabase.task;

import android.content.Context;

import java.util.List;

import br.com.andrecampos.marveldatabase.model.Hero;
import br.com.andrecampos.marveldatabase.webservice.WebServiceFactory;
import br.com.andrecampos.marveldatabase.webservice.interfaces.HeroWebService;
import br.com.andrecampos.marveldatabase.webservice.response.HeroResponse;
import retrofit2.Response;

/**
 * -- MarvelDatabase --
 * Autor: André Campos
 * E-mail: andrecampos.ds@gmail.com
 * Data: 30/09/16 - 00:43
 */

public class DetailHeroTask extends BaseTask<Hero> {

    private Long id;

    public DetailHeroTask(Context context, Long id) {
        // Executa o método construtor da classe ancestral
        super(context);
        // Define o valor dos atributos do objeto
        this.id = id;
    }

    @Override
    protected Hero doInBackground(Void... voids) {
        // Instancia o objeto responsável por realizar a comunicação com o webservice
        HeroWebService webService = WebServiceFactory.build(HeroWebService.class);
        // Define a variável que irá receber o retorno do webservice
        Response<HeroResponse> response;
        // Inicia um bloco seguro para realizar a chamada
        try {
            // Executa a chamada do webservice
            response = webService.get(id,
                    WebServiceFactory.createWebServiceParameters(this.getContext()))
                    .execute();
        } catch (Exception e) {
            // Imprime a pilha de exceções
            e.printStackTrace();
            // Retorna nulo, indicando que a função não foi realizada com sucesso
            return null;
        }
        // Verifica se ocorreu algum erro durante a execução do método
        if (!response.isSuccessful()) {
            // Retorna nulo, indicando que a função não foi realizada com sucesso
            return null;
        }
        // Recupera o objeto de response que possui os dados dos heróis
        HeroResponse heroResponse = response.body();
        // Recupera a lista de heróis
        List<Hero> list = heroResponse.getResults();
        // Verifica se a lista é nula
        if ((list == null) || (list.size() == 0)) {
            // Retorna nulo, indicando que a função não foi realizada com sucesso
            return null;
        }
        // Retorna o valor da função
        return list.get(0);
    }
}
