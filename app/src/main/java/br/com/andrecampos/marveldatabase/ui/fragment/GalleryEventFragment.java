package br.com.andrecampos.marveldatabase.ui.fragment;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import br.com.andrecampos.marveldatabase.R;
import br.com.andrecampos.marveldatabase.adapter.GalleryEventAdapter;
import br.com.andrecampos.marveldatabase.task.ListEventTask;
import br.com.andrecampos.marveldatabase.webservice.response.EventResponse;

/**
 * -- MarvelDatabase --
 * Autor: André Campos
 * E-mail: andrecampos.ds@gmail.com
 * Data: 30/09/16 - 12:44
 */

public class GalleryEventFragment extends BaseFragment implements
        ListEventTask.OnPostExecuteListener<EventResponse> {

    private RecyclerView recyclerView;
    private GalleryEventAdapter adapter;
    private ListEventTask task;

    private Long heroId;
    private int offset;
    private boolean allDataLoaded;

    public GalleryEventFragment() {

    }

    public void setHeroId(Long heroId) {
        // Informa o Id do herói
        this.heroId = heroId;
        // Sincroniza os dados dos eventos
        this.syncEvent();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        // Executa o método da classe ancestral
        super.onCreate(savedInstanceState);
        // Instancia um novo objeto do adapter
        this.adapter = new GalleryEventAdapter(this.getActivity().getApplicationContext(),
                null);
        // Define o valor inicial das variáveis
        this.offset = 0;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        // Renderiza o layout do fragmento
        final View layout = inflater.inflate(R.layout.fragment_gallery_event, container, false);
        // Realiza a inicialização das views
        this.initViews(layout);
        // Realiza as configurações do RecyclerView
        this.setupRecyclerView();
        // Retorna o layout configurado
        return layout;
    }

    /**
     * Inicializa as variáveis correspondentes às {@link android.view.View views} do layout.
     */
    private void initViews(@NonNull View layout) {
        // Instancia a variável com os componentes definidos no layout do fragmento
        this.recyclerView = (RecyclerView) layout.findViewById(R.id.recyclerView);
    }

    /**
     * Realiza as configurações do {@link RecyclerView}.
     */
    private void setupRecyclerView() {
        // Verifica se o recyclerView foi instanciado
        if (this.recyclerView != null) {
            // Instancia o gerenciador de layout do RecyclerView
            LinearLayoutManager layoutManager = new GridLayoutManager(this.getContext(), 2);
            // Adiciona o gerenciador de layouts ao RecyclerView
            this.recyclerView.setLayoutManager(layoutManager);
            // Configura o listener de scroll do recyclerview
            this.recyclerView.addOnScrollListener(this.createRecyclerViewScrollListener());
            // Atribui o adapter do RecyclerView
            this.recyclerView.setAdapter(this.adapter);
        }
    }

    /**
     * Cria o objeto responsável por acompanhar as mudanças realizadas no scroll do {@link RecyclerView}.
     * @return Um novo {@link android.support.v7.widget.RecyclerView.OnScrollListener}.
     */
    private RecyclerView.OnScrollListener createRecyclerViewScrollListener() {
        // Retorna uma nova instância do OnScrollListener
        return new RecyclerView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                // Executa o método da classe ancestral
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                // Executa o método da classe ancestral
                super.onScrolled(recyclerView, dx, dy);
                // Verifica se o scroll está sendo realizado para baixo
                if (dy > 0) {
                    // Verifica se o layoutManager do RecyclerView é uma instância de GridLayoutManager
                    if (recyclerView.getLayoutManager() instanceof GridLayoutManager) {
                        // Realiza um cast do LayoutManager
                        GridLayoutManager manager = (GridLayoutManager) recyclerView.getLayoutManager();
                        // Calcula o valor máximo que o limite deve possuir para carregar mais dados
                        int limit = Math.max(manager.getItemCount() - (manager.getChildCount() * 3), 0);
                        // Verifica se o limite foi excedido e se o limite é maior que zero
                        if ((limit <= manager.findFirstVisibleItemPosition()) && (limit > 0)) {
                            // Atualiza o valor do offset com o valor de registros no Recyclerview
                            offset = manager.getItemCount();
                            // Incrementa a lista de eventos
                            syncEvent();
                        }
                        // Verifica se a lista atingiu o final
                        else if ((manager.getItemCount() - manager.getChildCount()) == manager.findFirstVisibleItemPosition()) {
                            // Atualiza a lista de eventos
                            syncEvent();
                        }
                    }
                }
            }
        };
    }

    public void syncEvent() {
        // Verifica se o objeto de task é nulo ou não está em execução e se existe pendência de dados
        if (((this.task == null) || (this.task.getStatus() != AsyncTask.Status.RUNNING)) && (!this.allDataLoaded)) {
            // Instancia uma nova task atribuindo o offset atual
            this.task = new ListEventTask(this.getActivity().getApplicationContext(),
                    this.heroId,
                    this.offset);
            // Atribui o listener que será executado quando a rotina for realizada com sucesso
            this.task.setOnPostExecuteListener(this);
            // Inicia a execução da task
            this.task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
    }

    @Override
    public void onPostExecute(EventResponse result) {
        // Verifica se o adapter foi instanciado
        if (this.adapter != null) {
            // Adiciona o conteúdo recuperado na listagem de heróis
            this.adapter.addEvents(result.getResults());
            // Atualiza se todos os dados foram carregados
            this.allDataLoaded = result.isEof();
        }
    }
}
